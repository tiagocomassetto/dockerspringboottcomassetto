FROM openjdk:12
ADD target/docker-spring-boot-tcomassetto.jar docker-spring-boot-tcomassetto.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-tcomassetto.jar"]
